#!/usr/bin/env python3
###
#importing packages_________________________________________



print(0.0)
print(0.1)
print(0.2)
print(0.3)
import sys
sys.stdout.flush()


import tsfel
import statistics 
import zipfile
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import itertools as it
import scikitplot as skplt  # ROC
import seaborn as sns
# roc curve and auc score
from sklearn.datasets import make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

#import serial

from myUtils import * 

#keras
from keras.losses import categorical_crossentropy
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers import TimeDistributed , BatchNormalization
from keras.layers.recurrent import LSTM
from keras.layers import Dense, Conv1D, MaxPool2D, Flatten, Dropout,  Conv2D, MaxPooling2D
from keras.callbacks import EarlyStopping, TensorBoard , ModelCheckpoint, EarlyStopping
from keras.metrics import mean_absolute_percentage_error,mean_squared_error
from keras.optimizers import Adam, SGD, Nadam
from time import time
from keras.layers.advanced_activations import LeakyReLU, PReLU
import tensorflow as tf
from tensorflow.python.client import device_lib
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
from keras import *


#import for arduino
import serial

#count ms
import datetime

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

#Load the previously contructed model
#============================================================================

model = load_model('models\\CNN.best.hdf5', custom_objects={'mse':'mse','acc':'accuracy','mape': 'mape' }) # Gave an error when loading without 'custom_objects'.. fixed by https://github.com/keras-team/keras/issues/3911

# Compile with the same settings as it has been saved with earlier
#model.compile(loss='mse', metrics=['mse','accuracy','mape'], optimizer='sgd')
model.compile()

prediction = 0


def predictFall(result):
    global prediction
    
    X_values = np.array([result,])
    
    X_values = X_values.reshape( X_values.shape[0], X_values.shape[1],X_values.shape[2], 1)
    #start_time = datetime.datetime.now()
    Y_pred = model(X_values,training=False)
    #end_time = datetime.datetime.now()
    #print("execturion time: "+str((end_time - start_time).total_seconds()*1000))  
    Y_pred = np.array(Y_pred)
    if Y_pred[0][0]>0.5:
        predicted = "NEGATIVE"
    else:
        predicted = "POSITIVE"
    
    prediction = Y_pred[0][1]
    #print(str(Y_pred[0][1]))
    #print("Probability of negative: "+str(Y_pred[0][0]) + "  Probability of positive: "+str(Y_pred[0][1])+"  Predicted class: "+predicted)
    return prediction
    


#####Dynamic normalization
##### Normalize the DATASET by each sensor not by the whole matrix
theWindow = None
myWindow = list()
maxV = np.zeros(16,dtype="int32")
minV = np.zeros(16,dtype="int32")
prevMax = np.zeros(16,dtype="int32")
prevMin = np.zeros(16,dtype="int32")
res = np.zeros([100,16],dtype="float32")

def dynamicNormalizationPerColumnDS(row):
    global theWindow
    global myWindow
    global maxV
    global minV
    global prevMax
    global prevMin
    global res
    
    
    if len(myWindow) < 99:
        myWindow.insert(len(myWindow),row)
        return np.array([])
    elif len(myWindow) == 99:
        myWindow.insert(len(myWindow),row)
        theWindow = np.array(myWindow,dtype="float32")
        return np.array([])
    else:
        theWindow[:-1] = theWindow[1:]
        theWindow[-1] = row
    
        #normaliziraj
        maxV = np.max(theWindow,axis=0)
        minV = np.min(theWindow,axis=0)
        #minV = np.zeros(16,dtype="int32")
        
        index = np.zeros(16,dtype="int32")
        
        index = np.absolute(maxV - prevMax)
        
        index = index + np.absolute(minV - prevMin)
        
        axis = 0
        
        for v in index:
            
            if v > 0 :
                for j in range(100):
                     if (minV[axis]==0 and maxV[axis] == 0) or maxV[axis]<1000:
                         res[j][axis] = 0    
                     else:    
                         res[j][axis]=(int(theWindow[j][axis])-minV[axis])/(maxV[axis]-minV[axis])
            else:
                res[:-1][axis] = res[1:][axis]
                if (minV[axis]==0 and maxV[axis] == 0) or maxV[axis]<1000:
                         res[-1][axis] = 0    
                else:  
                    res[-1][axis] = (int(theWindow[-1][axis])-minV[axis])/(maxV[axis]-minV[axis])
            axis += 1
                
        prevMax = maxV
        prevMin = minV
        
        return res
        
        
##Povpreci napovedi v casovnem oknu
resWindow = np.zeros(25,dtype="float32")
resCounter = 0

def resultWindow(value):   
    global resCounter
    global resWindow
    resWindow[:-1] = resWindow[1:]
    resWindow[-1] = value
    
    if resCounter == 10:
        sys.stdout.flush()
        print(np.mean(resWindow))        
        resCounter=0
    else:
        resCounter+=1
    



#setup serial communication
ser = serial.Serial('/dev/ttyUSB0', 115200)
ser.flushInput()


theWindow = None
myWindow = list()
window = list()
predictionCounter = 0
####NAREDI NAPOVED VSAKIH 5 VREDNOSTI!!! vsakih 100ms



while True:
    try:
        ser_bytes = ser.readline()
        data_serial = ser_bytes[0:len(ser_bytes)-3].decode('utf-8')
        l=data_serial.split(sep=",")
        l=list(map(int,l))
        
        #test speed of recording
        ###
        #start_time = datetime.datetime.now()
        result = dynamicNormalizationPerColumnDS(l)
        if predictionCounter == 5:
            predictionCounter = 0
            if result.size != 0:
                sys.stdout.flush()
                print(predictFall(result))
        else:
            predictionCounter+=1
      
        #end_time = datetime.datetime.now()
        #print("execturion time: "+str((end_time - start_time).total_seconds()*1000))      
    
                        
    except:
        print("Keyboard Interrupt")
        break


