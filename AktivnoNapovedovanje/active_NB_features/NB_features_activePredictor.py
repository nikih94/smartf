#!/usr/bin/env python3
###
#importing packages_________________________________________



print(0.0)
print(0.1)
print(0.2)
print(0.3)
import sys

#importing packages_________________________________________

import tsfel
import statistics 
import zipfile
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools as it
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB


from myUtils import * 

#to import classifier
import joblib



#import for arduino
import serial

#count ms
import datetime

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

#Load the previously contructed model
#============================================================================
model = joblib.load('naiveBayes.pkl')





def predictFall(result):
    return model.predict(result)
    


#####Dynamic normalization
##### Normalize the DATASET by each sensor not by the whole matrix
theWindow = np.zeros([100,16],dtype="int32")
theWindowCounter = 0

def buildWindow(row):
    global theWindow
    global theWindowCounter
    theWindow[:-1] = theWindow[1:]
    theWindow[-1] = row 
    if theWindowCounter == 100:
        return theWindow
    else:
        theWindowCounter+=1
        return np.array([])
        
        
##Povpreci napovedi v casovnem oknu
resWindow = np.zeros(100,dtype="int32")



def resultWindow(value):   
    global resWindow
    resWindow[:-1] = resWindow[1:]
    resWindow[-1] = value
    
    num = resWindow.sum()
    
    if num < 17:
        return (num / 25) * 0.5
    else:
        return (num / 100) + 0.34
    
    



myWindow = list()
window = list()
predictionCounter = 0
####NAREDI NAPOVED VSAKIH 5 VREDNOSTI!!! vsakih 100ms

for line in sys.stdin:
    line = line.strip()
    data = line.split(sep=",")
    l=list(map(int,data)) 
    #test speed of recording
    ###
    #start_time = datetime.datetime.now()
    
    value = predictFall(np.array([l]))[0]
    
    if predictionCounter == 5:
        sys.stdout.flush()
        print(resultWindow(value))
        predictionCounter=0
    else:
        predictionCounter+=1
        resultWindow(value)
    

    #end_time = datetime.datetime.now()
    #print("execturion time: "+str((end_time - start_time).total_seconds()*1000))      



