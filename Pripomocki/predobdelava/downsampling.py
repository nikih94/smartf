# -*- coding: utf-8 -*-




#importing packages_________________________________________

import tsfel
import statistics 
import zipfile
import math
import numpy as np
import itertools as it
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from tqdm import tqdm
import dask.dataframe as dd



path = "/home/niki/Desktop/tesi/dataZaObdelavo"


df = pd.read_csv(path+"/fallData.csv")

df = df.drop(["person_ID","fall_Category","time_10ms"],axis=1)



###downsample to 50hz by dropping every other row 

indexList = list()

for i in range(len(df.index+1)):
    if i%2==0:
        #row1 = df.iloc[i]
        indexList.insert(len(indexList),i)

downSampledDF = df.drop(indexList)
    

downSampledDF.to_csv(path+'/downSampledData.csv', index=False)
