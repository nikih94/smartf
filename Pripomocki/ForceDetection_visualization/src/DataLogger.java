import com.fazecast.jSerialComm.SerialPort;

import java.util.Scanner;

public class DataLogger
        extends Thread
{
    public static SerialPort port;
    public double delta = 3.5D;

    public void openSerialPort() {
        SerialPort[] available_ports = SerialPort.getCommPorts();

        for (int i = 0; i < available_ports.length; i++) {
            System.out.println(String.valueOf(i) + " : " + available_ports[i]);
        }
        //kommentiraj za feature extraction iz file ------------------------------------------------------------------------
        port = available_ports[Main.port_id];
        if (port != null) {
            port.setComPortTimeouts(4096, 0, 0);
            System.out.println(port.openPort());
            if (!port.openPort()) {
                System.err.println("Unable to open the port.");
            }
        }
        //kommentiraj za feature extraction iz file ------------------------------------------------------------------------
        port.setBaudRate(115200);
    }

    public void run() {
        for (int i = 0; i < Main.sensor_data[1].length; i++) {
            Main.sensor_data[1][i] = 1000.0F;
        }

        //postavi port == null za feature extraction iz file, drugace port != null   --------------------------------------------------------
        if (port != null) {
            Scanner s = null;
            /// Branje iz ARDUINO
            s = new Scanner(port.getInputStream());




            float[] previous_datapoints = new float[16];

            while (s.hasNextLine()) {
                String line = s.nextLine();
                line = line.substring(0, line.length() - 1);       ////PREGLEDI DA JE ODKOMENTIRANA KO RABIS ARDUINO
                String[] datapoints = line.split(",");
                float[] currentDatapoints = new float[16];

                if (datapoints.length > 16) {
                    System.out.println("Malformed payload by the controller");
                } else {

                    for (int i = 0 ; i < datapoints.length; i++){
                        currentDatapoints[i] = Float.parseFloat(datapoints[i]);
                    }

                    //drugo
                    for (int i = 0; i < datapoints.length; i++) {
                        float force = currentDatapoints[i];
                        if (force < 0.0F)
                            force = Math.abs(force);
                        if (Main.sensor_data[1][i] < force) {
                            Main.sensor_data[1][i] = Main.sensor_data[1][i] + (Main.sensor_data[1][i] + force) / 2.0F;
                        }
                        if (force - Main.sensor_data[2][i] > 0.0F) {
                            if ((force - Main.sensor_data[2][i]) / Main.sensor_data[1][i] <= 1.0F)
                            {

                                Main.sensor_data[0][i] = (force - Main.sensor_data[2][i]) / Main.sensor_data[1][i];
                            }
                        } else {
                            Main.sensor_data[0][i] = 0.0F;
                        }

                        if (Math.abs(currentDatapoints[i] - previous_datapoints[i]) < this.delta && currentDatapoints[i] < 1000.0F) {
                            Main.sensor_data[2][i] = currentDatapoints[i] ;
                        }
                        previous_datapoints[i] = currentDatapoints[i] ;
                    }
                }
                System.out.print("Line of text from serial port: ");
                for (int i = 0 ; i < currentDatapoints.length ; i++){
                    System.out.print(Integer.toString((int)currentDatapoints[i])+",");
                }
                System.out.println();
            }
        } else {
            System.out.println("Unable to read from serial port..");
        }
    }

    public void propagateForce(float[] sensor_data) {}
}