#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 11 11:57:06 2020

@author: niki
"""

#importing packages_________________________________________

#import tsfel
import statistics 
import zipfile
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools as it
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB


from myUtils import * 

#to save classifier
import joblib


#import data:_______________________________________________
positive_path="./data/features/fallData.csv" 


##Create positive dataset
X_train , Y_train = createPositiveSetSimpleNoLabels(positive_path)



###Create test set from Positive test data and delete from Train set
X_test,Y_test = createTestfromTrainDelete(X_train,Y_train,20)




##rotate also the test set
rotateByMatrix(X_test,Y_test)


####Rotate the data by different angles
rotateByMatrix(X_train,Y_train)


####Addd also negative data
negative_path= "./data/features/negativeData.csv"

X_trainN = list()
Y_trainN = list()

##Create negative dataset
addNegativeSet(X_trainN,Y_trainN,negative_path,100,99,150)


####Addd also negative Stoj data
stoj_path= "./data/features/stoj.csv"

X_stojN = list()
Y_stojN = list()

##Create empty dataset
addNegativeSet(X_stojN,Y_stojN,stoj_path,100,2,47)


##Insert in test set data from stoj set
insertTestfromTrainDelete(X_stojN, Y_stojN, X_test, Y_test, 7)


####Add negative empty floor data
empty_floor_path= "./data/features/emptyFloor.csv"

X_empty = list()
Y_empty = list()

##Create empty dataset
addNegativeSet(X_empty,Y_empty,empty_floor_path,100,2,10)



##Insert in test set data from negative set
insertTestfromTrainDelete(X_trainN, Y_trainN, X_test, Y_test, 50)



##rotate also negative
rotateByMatrix(X_trainN,Y_trainN,negative=True)





###Insert test data from precious
testPath= "./data/features/testData.csv"
X_testPrec = list()
Y_testPrec = list()
insertTestFromFile(testPath, X_testPrec, Y_testPrec,withoutID=True)


rotateByMatrix(X_testPrec,Y_testPrec,negative=True)

X_test , Y_test = mergePandN(X_test, Y_test, X_testPrec, Y_testPrec)

###Dodaj se empty v test set
X_test , Y_test = mergePandN(X_test,Y_test,X_empty,Y_empty)

###Merge positive and negative set
X_train , Y_train = mergePandN(X_train, Y_train, X_trainN, Y_trainN)





### pojdi skozi dataset in ce je Y na 1 pregledi ali vec kot 1 ali 2 aktivirana value
###pretvori 3D list v 2D list

values=0
req = 1 #stevilo zahtevanih aktiviranih vrednosti

newX = list() ##train set
newY = list() ##train set Y
iiix=0
iiiy=0

for i in range(len(Y_train)):
    if Y_train[i] == 1:
        for j in range(len(X_train[i])):
            for k in range(len(X_train[i][j])):
                if int(X_train[i][j][k]) > 0:
                    values += 1
            if values >= req:
                #vstavi vrstico v podatkovno mnozico
                newX.insert(len(newX),X_train[i][j])
                newY.insert(len(newY),1)
                iiix+=1
            values = 0
    else:
        for j in range(len(X_train[i])):
            newX.insert(len(newX),X_train[i][j])
            newY.insert(len(newY),0)
            iiiy+=1

print("st pozitivnih primerov " + str(iiix))
print("st negativnih primerov " + str(iiiy))



###################KLICI NA KONCUUUUU
newX = np.array(newX,dtype="int64")
newY = np.array(newY,dtype="int64")  
testY = np.array(Y_test,dtype="int64")  
############################################################


model = GaussianNB()
model.fit(newX, newY);

#print("meja,tocnost,specificnost,senzitivnost")

####Testiraj na time window
#meja definira stevilo vrstic time windowa dolgega 100 ki morajo biti pozitivne za dolociti pozitiven razred
#for h in range(0,70,5):   
positive = 0

meja = 17

Y_pred = list()
Y_prob = list()

for m in range(len(X_test)):    
    for r in range(len(X_test[m])):
        row = np.array(X_test[m][r],dtype="int64")
        y_predicted = model.predict([row])[0]
        positive += y_predicted 
    if positive > meja:
        Y_pred.insert(len(Y_pred),1)
    else:
        Y_pred.insert(len(Y_pred),0)
    prob = positive / 50
    if prob > 1:
        prob = 1
    Y_prob.insert(len(Y_prob),prob)
    positive = 0
    
        


TN=0
TP=0
FN=0
FP=0

for i in range(len(Y_pred)):
    if Y_pred[i] == 0 :
        if testY[i] == 0:
            TN+=1
        else:
            FN+=1
    else:
        if testY[i] == 1:
            TP+=1
        else:
            FP+=1
            
            
tocnost = (TN + TP) / (FN + FP + TN + TP)
print("Tocnost: "+str(tocnost))
senzitivnost = TP / (TP + FN)
print("Senzitivnost: "+str(senzitivnost))
specificnost = TN / (TN + FP)
print("Specificnost: "+str(specificnost))
print("True positive: "+str(TP))
print("False positive: "+str(FP))
print("True negative: "+str(TN))
print("False negative: "+str(FN))


    #print("meja: "+str(meja)+" ------------------------------------")
    #print(str(meja)+","+str(tocnost)+","+str(specificnost)+","+str(senzitivnost))
    #####New predictor

# save the classifier
joblib.dump(model, 'naiveBayes.pkl', compress=9)



#ROC analiza
#=============================================================================
def plot_roc_curve(fpr, tpr):
    plt.plot(fpr, tpr, color='orange', label='ROC')
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend()
    plt.show()
    
    
fpr, tpr, thresholds = roc_curve(Y_test,Y_prob)
plot_roc_curve(fpr,tpr)


auc = roc_auc_score(Y_test,Y_prob)
print('AUC: %.2f' % auc)

#=============================================================================



