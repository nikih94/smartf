#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 15:46:28 2020

@author: niki
"""
### OPIS NN
# najprej prejmemo nove podat

#FINISHED
#Tocnost: 0.9207920792079208
#Senzitivnost: 0.8571428571428571
#Specificnost: 1.0
#problem podatki false niso najboljsi 


###
#importing packages_________________________________________

#import tsfel
import statistics 
import zipfile
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools as it
import scikitplot as skplt  # ROC
import seaborn as sns
# roc curve and auc score
from sklearn.datasets import make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

#import serial

from myUtils import * 

#keras
from keras.losses import categorical_crossentropy
from keras.callbacks import SGDRScheduler
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.layers import TimeDistributed , BatchNormalization
from keras.layers.recurrent import LSTM
from keras.layers import Dense, Conv1D, MaxPool2D, Flatten, Dropout,  Conv2D, MaxPooling2D
from keras.callbacks import EarlyStopping, TensorBoard , ModelCheckpoint, EarlyStopping
from keras.metrics import mean_absolute_percentage_error,mean_squared_error
from keras.optimizers import Adam, SGD, Nadam
from time import time
from keras.layers.advanced_activations import LeakyReLU, PReLU
import tensorflow as tf
from tensorflow.python.client import device_lib
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
from keras import *





#import data:_______________________________________________
positive_path="./data/features/fallData.csv" 


##Create positive dataset
X_train , Y_train = createPositiveSetSimpleNoLabels(positive_path)


###Create test set from Positive test data and delete from Train set
X_test,Y_test = createTestfromTrainDelete(X_train,Y_train,20)



####Shift the datatseettt
#dataShifter(X_train, Y_train)


#multiplySet(X_train,Y_train,1)

##rotate also the test set
rotateByMatrix(X_test,Y_test)


####Rotate the data by different angles
rotateByMatrix(X_train,Y_train)


####Addd also negative data
negative_path= "./data/features/negativeData.csv"

X_trainN = list()
Y_trainN = list()

##Create negative dataset
addNegativeSet(X_trainN,Y_trainN,negative_path,100,99,90)


####Addd also negative Stoj data
stoj_path= "./data/features/stoj.csv"

X_stojN = list()
Y_stojN = list()

##Create empty dataset
addNegativeSet(X_stojN,Y_stojN,stoj_path,100,2,47)


##Insert in test set data from stoj set
insertTestfromTrainDelete(X_stojN, Y_stojN, X_test, Y_test, 7)


####Add negative empty floor data
empty_floor_path= "./data/features/emptyFloor.csv"

X_empty = list()
Y_empty = list()

##Create empty dataset
addNegativeSet(X_empty,Y_empty,empty_floor_path,100,2,40)



##Insert in test set data from negative set
insertTestfromTrainDelete(X_trainN, Y_trainN, X_test, Y_test, 50)


###Multiply the empty
#multiplySet(X_empty,Y_empty,1)

###Zdruzi negative in empty floor
X_trainN , Y_trainN = mergePandN(X_trainN,Y_trainN,X_empty,Y_empty)

###Ker si ga multiplikiral
X_empty = list()
Y_empty = list()

##Create empty dataset
addNegativeSet(X_empty,Y_empty,empty_floor_path,100,2,10)


###Zdruzi negative in empty floor
X_trainN , Y_trainN = mergePandN(X_trainN,Y_trainN,X_stojN,Y_stojN)



##rotate also negative
rotateByMatrix(X_trainN,Y_trainN,negative=True)





###Insert test data from precious
testPath= "./data/features/testData.csv"
X_testPrec = list()
Y_testPrec = list()
insertTestFromFile(testPath, X_testPrec, Y_testPrec,withoutID=True)


rotateByMatrix(X_testPrec,Y_testPrec,negative=True)

X_test , Y_test = mergePandN(X_test, Y_test, X_testPrec, Y_testPrec)

###Dodaj se empty v test set
X_test , Y_test = mergePandN(X_test,Y_test,X_empty,Y_empty)

###Merge positive and negative set
X_train , Y_train = mergePandN(X_train, Y_train, X_trainN, Y_trainN)








###shuffle the datatset
shuffleDataWithseed(X_train,Y_train,3000)




###################KLICI NA KONCUUUUU
X_train = np.array(X_train,dtype="int32")
Y_train = np.array(Y_train,dtype="int32")  
X_test = np.array(X_test,dtype="int32")
Y_test = np.array(Y_test,dtype="int32")  
############################################################


####Copy Y_test for later ROC analyses
y_true = Y_test
####


    
#####ONE HOT ENCODE Y
Y_train = oneHotEncoding(Y_train)
Y_test = oneHotEncoding(Y_test)



#####Adding CHANNEL dimension to the X data.
X_train = X_train.reshape(X_train.shape[0], X_train.shape[1], X_train.shape[2], 1)
X_test = X_test.reshape(X_test.shape[0], X_test.shape[1], X_test.shape[2], 1)
X_train.shape
#################




    
    
##################THE CNN------------------------------





def create_model(X_train, y_train, X_test, y_test):
    input_shape = (X_train.shape[1], X_train.shape[2], X_train.shape[3])

    model = Sequential()
    
    ks1_first = 5
    ks1_second = 5
    
    ks2_first = 3
    ks2_second = 3
    
    ks3_first = 3
    ks3_second = 3
    
    #ideal filters = 128,256,512
    
    model.add(Conv2D(filters=32,
                     kernel_size=(ks1_first,ks1_second),
                     input_shape=input_shape,
                     padding='same',
                     activation='relu'
                     ))
    
    model.add(Dropout(0.8))
    
    model.add(MaxPooling2D(pool_size=(2,1),strides=None,padding='valid'))
    
    
    
    model.add(Conv2D(filters=32,
                     kernel_size=(ks2_first,ks2_second),
                     input_shape=input_shape,
                     padding='same',
                     activation='relu'
                     ))
       
    model.add(Dropout(0.7))
    
    
    model.add(MaxPooling2D(pool_size=(2,1),strides=None,padding='valid'))
    
    
    # model.add(Conv2D(filters=64,
    #                  kernel_size=(ks3_first,ks3_second),
    #                  input_shape=input_shape,
    #                  padding='same',
    #                  activation='relu'
    #                  ))
       
    # model.add(Dropout(0.5))
    
    
    # model.add(MaxPooling2D(pool_size=(2,1),strides=None,padding='valid'))
    
    
    model.add(Flatten())
    #model.add(Dense(64,activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(16,activation='relu'))
    model.add(Dense(2))
    model.add(Activation('softmax'))
    
    
    return model





epochs = 10
bs = 32
lr = 0.01 #6e-4
print(bs)


model = create_model(X_train, Y_train, X_test, Y_test)

# 0.05 0.9 0 True
sgd = SGD(lr=0.1, momentum=0.9, decay=0, nesterov=True) # sgd in general yields better results, but needs a lot of tweeking and is slower
adam = Adam(lr=lr)
nadam = Nadam(lr=lr)

model.compile(optimizer='nadam', loss = categorical_crossentropy, metrics=['mape','accuracy', 'mse'])


###FIT the model
early_stopping_monitor = EarlyStopping(patience=5000)



# This is used to save the best model, currently monitoring val_mape
# checkpoint
filepath="models\\CNN.best.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')


epoch_size = 56
#neznam ce obstaja ta roba
#schedule = SGDRScheduler(min_lr= 9e-7 ,max_lr= 4.3e-3 ,steps_per_epoch=np.ceil(epoch_size/bs),lr_decay=0.9, cycle_length=5,mult_factor=1.5)



model.fit(X_train, Y_train, epochs=epochs, shuffle=False,batch_size=bs, validation_split=0.2,verbose=1, callbacks=[ early_stopping_monitor, checkpoint])

print(model.summary())




################Test model

#load a previously saved model
#============================================================================
# Load the architecture
model = load_model('models\\CNN.best.hdf5', custom_objects={'mse':'mse','acc':'accuracy','mape': 'mape' }) # Gave an error when loading without 'custom_objects'.. fixed by https://github.com/keras-team/keras/issues/3911

# Compile with the same settings as it has been saved with earlier
model.compile(loss='mse', metrics=['mse','accuracy','mape'], optimizer='nadam')

print('FINISHED')
#=============================================================================

Y_pred = model.predict(X_test)

##Izracunaj tocnost,specificnost in senzitivnost modela

TN=0
TP=0
FN=0
FP=0

for i in range(Y_pred.shape[0]):
    if Y_pred[i][0] >= Y_pred[i][1]:
        if Y_test[i][0] > Y_test[i][1]:
            TN+=1
        else:
            FN+=1
    else:
        if Y_test[i][0] < Y_test[i][1]:
            TP+=1
        else:
            FP+=1
            
            
tocnost = (TN + TP) / (FN + FP + TN + TP)
print("Tocnost: "+str(tocnost))
senzitivnost = TP / (TP + FN)
print("Senzitivnost: "+str(senzitivnost))
specificnost = TN / (TN + FP)
print("Specificnost: "+str(specificnost))

print("True positive: "+str(TP))
print("False positive: "+str(FP))
print("True negative: "+str(TN))
print("False negative: "+str(FN))



#ROC analiza
#=============================================================================
def plot_roc_curve(fpr, tpr):
    plt.plot(fpr, tpr, color='orange', label='ROC')
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend()
    plt.show()
    
    
fpr, tpr, thresholds = roc_curve(y_true,Y_pred[:,1])
plot_roc_curve(fpr,tpr)


auc = roc_auc_score(y_true,Y_pred[:,1])
print('AUC: %.2f' % auc)

#=============================================================================













