import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Registracija extends Thread {




    //Avtomaticno spremeni cas registracije
    public boolean casRegProtokol = false;
    public int interval = 120;
    public int zadnji67 = 10;


    //NASTAVI cas registracije tudi za hojo in druge teste
    public long regTime = interval * 1000; //koliko casa bo registriralo padec v ms

    //pot do folderjev testirancev
    public String path = "/home/niki/Desktop/tesi/dataCollection/";

    ///Ime oziroma nickname osebe (to bo folder testiranca ) -> ustvari že prej vse folderje
    public String oseba = "negativeData";

    ///Ime oziroma tip padca
    public String name = "stoj";



    //opcije z retry
    public String options = "";


    public int numFiles = 1;

    //za pisanje na file
    public File file = null;
    public FileWriter fw = null;
    public PrintWriter pw = null;

    //progress count
    public int progress = 0;

    public volatile long targetTime = 0;
    public Scanner scan = new Scanner(System.in);

    public Registracija(){
        System.out.println("Program za registracijo podatkov");

    }

    public void pisiPodatke(String line){

        if(System.currentTimeMillis() < targetTime){//registriraj

            pw.println(line);
            pw.flush();

            progress++;

            if(  progress > 10 ) {
                System.out.print(".");
                progress = 0;
            }
            //System.out.println("Line of text from serial port: " + line);
        }//drugace ne registrirat



    }


    public void run() {


        while (true) {

            file = new File(path + oseba + "/" + name + options + numFiles);

            if (!file.exists()) { //ustvari file


                ///ce je aktivirana opcija za cas registracije na podlagi protokola se izvee to!
                if(casRegProtokol){
                    if(numFiles == 6 || numFiles == 7){
                        regTime = zadnji67 * 1000;
                    }else{
                        regTime = interval * 1000;
                    }
                }

                System.out.println("Pritisni ENTER za začet registracijo padca st: "+ numFiles +" , ki traja: " + (regTime / 1000) + "s " + "   Na lokaciji: " + path + oseba + "/" + name + options + numFiles);
                scan.nextLine();

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Registriram.....");
                Toolkit.getDefaultToolkit().beep();

                try { // definiraj writer
                    fw = new FileWriter(file);
                    pw = new PrintWriter(fw);
                } catch (IOException e) {
                    e.printStackTrace();
                }



                //start registracije
                targetTime = System.currentTimeMillis() + regTime;

                try {
                    Thread.sleep(regTime + 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println();
                Toolkit.getDefaultToolkit().beep();

                //zapri writer

                pw.close();

                //vprasaj ali zelis shraniti


                boolean repeate = false;
                do {
                    System.out.println("Zelis shraniti registracijo? -> y = shrani     n = zbrisi   r = shrani in poskusi ponovno");
                    repeate = false;

                    char input = scan.nextLine().charAt(0);

                    if (input == 'y') {
                        numFiles++; //incrementiraj stevilko fila
                        System.out.println("Shranil");
                        options = "";

                    } else if (input == 'n') {
                        if (file.delete()) {               //returns Boolean value
                            System.out.println(file.getName() + " deleted");   //getting and printing the file name
                        } else {
                            System.out.println("failed");
                        }

                    } else if (input == 'r') {
                        System.out.println("Shranil in poskusi ponovno");
                        options += "r";
                    } else {
                        repeate = true;
                    }


                } while (repeate);


            } else { // file ze obstaja inkrementiraj vrednost numFiles in ponovi zanko
                System.out.println("Datoteka ze obstaja");
                numFiles++;
            }


        }

    }


}
