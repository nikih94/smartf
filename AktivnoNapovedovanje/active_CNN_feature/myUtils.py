# -*- coding: utf-8 -*-


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 11 11:57:06 2020

@author: niki
"""

#importing packages_________________________________________

import tsfel
import statistics 
import zipfile
import math
import numpy as np
import itertools as it
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from tqdm import tqdm
import dask.dataframe as dd
import random

###############BASICALLY NEW MYUTILS-----------------------------------------------------

###Only import positive dataset

def createPositiveSetSimple(path):
        #import data:_______________________________________________
    file1 = open(path, 'r') 
    Lines = file1.readlines() 
    
    ###shrani podatke v pravilno obliko v list
    X_train = list()
    fall = list()
    row = list()
    Y_train = list()
    
    for line in Lines: 
        if line.strip() == "next":
            Y_train.insert(len(Y_train),1)
            X_train.insert(len(X_train),fall)
            fall = list()
        else:
            line = line.strip().split(sep=",")
            row = list()
            for el in line:
                row.insert(len(row),el)
            fall.insert(len(fall),row)

    return X_train, Y_train



### INSERT in TEST data from Training data and delete records from TRAIN
# samples - how much data to create
def insertTestfromTrainDelete(X_train,Y_train,X_test,Y_test,samples):
    e = int(len(X_train)/samples)
    indexes = list()
    
    for i in range(samples):
        X_test.insert(len(X_test),X_train[e*i])
        Y_test.insert(len(Y_test),Y_train[e*i])
        indexes.insert(len(indexes),e*i)
        
    for index in sorted(indexes, reverse=True):
        del X_train[index]
        del Y_train[index]
        
        
        
###### INSERT test examples from file
def insertTestFromFile(path,X_test,Y_test,withoutID=False):
    #import data:_______________________________________________
    file1 = open(path, 'r') 
    Lines = file1.readlines() 
    
    ###shrani podatke v pravilno obliko v list
    fall = list()
    row = list()
    
    for line in Lines: 
        if line.strip() == "next":
            Y_test.insert(len(Y_test),0)
            X_test.insert(len(X_test),fall)
            fall = list()
        else:
            line = line.strip().split(sep=",")
            row = list()
            first = True
            for el in line:
                if first and not withoutID: #skip first value of line which is ID
                    first = False
                else:    
                    row.insert(len(row),el)
            fall.insert(len(fall),row)
        


###### Merge positive and negative dataset

def mergePandN(X_pos,Y_pos,X_neg,Y_neg):
    X_train = list()
    Y_train = list()
    for m in X_pos:
        X_train.insert(len(X_train),m)
    for v in Y_pos:
        Y_train.insert(len(Y_train),v)
    for m in X_neg:
        X_train.insert(len(X_train),m)
    for v in Y_neg:
        Y_train.insert(len(Y_train),v)
        
    return X_train, Y_train



#### Rotate the data by different angles
###Negative defines if we are inserting negative data to be rotated
def rotateByMatrix(X_train,Y_train,negative=False): 
        if negative:
            value = 0
        else:
            value = 1
        orig_len = len(X_train)
        for k in range(1,4):
            for i in range(orig_len):
                fall = X_train[i]
                rotatedFall = list()
                Y_train.insert(len(Y_train),value)
                for row in fall:
                    m = np.array([[row[0],row[1],row[2],row[3]],[row[4],row[5],row[6],row[7]],[row[8],row[9],row[10],row[11]],[row[12],row[13],row[14],row[15]]]) 
                    line = np.rot90(m,k).ravel()
                    rotatedFall.insert(len(rotatedFall),line.tolist())
                X_train.insert(len(X_train),rotatedFall)


##########################ENd of new myUtils-------------------------------------------

####Create ositive dataSet
# path to positive dataset
# rotate if you want to perform rotation on data
# multiply if you want to define data multiplication -> you must specify multyvalue as a faktor of times you want to multiply the dataset


def createPositiveSet(path,multyValue=0,rotate=False,multiply=False):
    #import data:_______________________________________________
    file1 = open(path, 'r') 
    Lines = file1.readlines() 
    
    ###shrani podatke v pravilno obliko v list
    X_train = list()
    fall = list()
    row = list()
    Y_train = list()
    
    for line in Lines: 
        if line.strip() == "next":
            Y_train.insert(len(Y_train),1)
            X_train.insert(len(X_train),fall)
            fall = list()
        else:
            line = line.strip().split(sep=",")
            row = list()
            for el in line:
                row.insert(len(row),el)
            fall.insert(len(fall),row)

    #rotiraj podatke
    if rotate:
        orig_len = len(X_train)
        for k in range(1,4):
            for i in range(orig_len):
                fall = X_train[i]
                rotatedFall = list()
                Y_train.insert(len(Y_train),1)
                for row in fall:
                    m = np.array([[row[0],row[1],row[2],row[3]],[row[4],row[5],row[6],row[7]],[row[8],row[9],row[10],row[11]],[row[12],row[13],row[14],row[15]]]) 
                    line = np.rot90(m,k).ravel()
                    rotatedFall.insert(len(rotatedFall),line)
                X_train.insert(len(X_train),rotatedFall)
                
    if multiply:
        ##Multiply the data
        orig_len = len(X_train)
        for n in range(multyValue):
            for i in range(orig_len):
                fall = X_train[i]
                Y_train.insert(len(Y_train),1)
                X_train.insert(len(X_train),fall)
           
    return X_train, Y_train


#### Add negative data to training set
# X_train the training setattributes
# Y_train class attribute of training set
# path - path to file
# windowSize set to window Size of positive dataset (100)
# stride - how much will move the window
#length define max number of negative values
# removeID definira odstranitev colone id 
######PAZIII!! podatki morajo imeti header : 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,class

def addNegativeSet(X,Y,myPath,windowSize, stride,length,removeID = False):    
    ####Addd also negative data
    df_negative = pd.read_csv(myPath)
    if removeID:
        df_negative = df_negative.drop(columns="neg_ID")
    num = 0
    idx = 0 
    while idx  < len(df_negative.index)-windowSize and num < length :
        matrix = list()
        for s in range(windowSize):
            line = list()
            for el in range(16):
                line.insert(len(line),df_negative[str(el)][idx])
            matrix.insert(len(matrix),line)
            idx+=1
        idx-=windowSize - stride #tako naredimo vec podatkov
        num +=1
        Y.insert(len(Y),0)
        X.insert(len(X),matrix)
    print(idx)
    
        


#Shuffle the data for a defined SEED and for a selected number of shuffles over the DF
def shuffleDataWithseed(X_train,Y_train,numOfShuffles):
    random.seed(1234)
    startLength = len(X_train)
    for i in range(numOfShuffles):
        for p in range(len(X_train)):
            index2 = random.randint(1,startLength-1)
            X_train[p],X_train[index2] = X_train[index2],X_train[p]
            Y_train[p],Y_train[index2] = Y_train[index2],Y_train[p]
    
    


### CREATE TEST data from Training data
# samples - how much data to create
def createTestfromTrain(X,Y,samples):
    X_test = list()
    Y_test = list()
    e = int(len(X)/samples)
    
    for i in range(samples):
        X_test.insert(len(X_test),X[e*i])
        Y_test.insert(len(Y_test),Y[e*i])
        
    return X_test,Y_test


### CREATE TEST data from Training data and delete records from TRAIN
# samples - how much data to create
def createTestfromTrainDelete(X_train,Y_train,samples):
    X_test = list()
    Y_test = list()
    e = int(len(X_train)/samples)
    indexes = list()
    
    for i in range(samples):
        X_test.insert(len(X_test),X_train[e*i])
        Y_test.insert(len(Y_test),Y_train[e*i])
        indexes.insert(len(indexes),e*i)
        
    for index in sorted(indexes, reverse=True):
        del X_train[index]
        del Y_train[index]
        
    return X_test,Y_test


### APPEND TEST data from Training data
# samples - how much data to create
def appendTestfromTrain(X_test,Y_test,X,Y,samples):
    e = int(len(X)/samples)
    
    for i in range(samples):
        X_test.insert(len(X_test),X[e*i])
        Y_test.insert(len(Y_test),Y[e*i])




#### One Hot Encoding for data
def oneHotEncoding(data):
    shape = (data.size,data.max()+1)
    one_hot = np.zeros(shape,dtype="int64")
    rows = np.arange(data.size)
    one_hot[rows,data]=1
    return one_hot
    



##### Normalize the DATASET by each sensor not by the whole matrix
def normalizePerColumnDS(X_train):
    for m in X_train:
        minV = np.full(16,1000000,dtype="int32")
        maxV = np.zeros(16,dtype="int32")
        for r in m:
            for i in range(16):
                if int(r[i]) < minV[i]:
                    minV[i] = int(r[i])
                if int(r[i]) > maxV[i]:
                    maxV[i] = int(r[i])
        for r in m:
            for i in range(16):
                if minV[i]==0 and maxV[i] == 0:
                    r[i] = 0    
                else:    
                    r[i]=(int(r[i])-minV[i])/(maxV[i]-minV[i])
            



############Reshape dataset for CNN

def df_to_cnn_rnn_format(df, test_size=0.5, look_back=5, target_column='target', scale_X=True):
    """
    Input is a Pandas DataFrame. 
    Output is a np array in the format of (samples, timesteps, features).
    Currently this function only accepts one target variable.

    Usage example:

    # variables
    df = data # should be a pandas dataframe
    train_size = 0.5 # percentage to use for training
    target_column = 'c' # target column name, all other columns are taken as features
    scale_X = False
    look_back = 5 # Amount of previous X values to look at when predicting the current y value
    """
    df = df.copy()

    # Make sure the target column is the last column in the dataframe
    df['target'] = df[target_column] # Make a copy of the target column, this places the new 'target' column at the end of all the other columns
    df = df.drop(columns=[target_column]) # Drop the original target column
    
    target_location = df.shape[1] - 1 # column index number of target
    split_index = int(df.shape[0]*test_size) # the index at which to split df into train and test
    
    # ...train
    X_train = df.values[:split_index, :target_location]
    y_train = df.values[:split_index, target_location]

    # ...test
    X_test = df.values[split_index:, :target_location] # original is split_index:-1
    y_test = df.values[split_index:, target_location] # original is split_index:-1

    # Scale the features
    if scale_X:
        scalerX = StandardScaler(with_mean=True, with_std=True).fit(X_train)
        X_train = scalerX.transform(X_train)
        X_test = scalerX.transform(X_test)
        
    # Reshape the arrays
    samples = len(X_train)
    num_features = target_location # All columns before the target column are features

    samples_train = X_train.shape[0] - look_back
    X_train_reshaped = np.zeros((samples_train, look_back, num_features)) # Initialize the required shape with an 'empty' zeros array.
    y_train_reshaped = np.zeros((samples_train))

    for i in range(samples_train):
        y_position = i + look_back
        X_train_reshaped[i] = X_train[i:y_position]
        y_train_reshaped[i] = y_train[y_position]


    samples_test = X_test.shape[0] - look_back
    X_test_reshaped = np.zeros((samples_test, look_back, num_features))
    y_test_reshaped = np.zeros((samples_test))

    for i in range(samples_test):
        y_position = i + look_back
        X_test_reshaped[i] = X_test[i:y_position]
        y_test_reshaped[i] = y_test[y_position]
    
    return X_train_reshaped, y_train_reshaped, X_test_reshaped, y_test_reshaped



####################################################



# =============================================================================
# 
# 
# seed = 42
# 
# """
# For info on batch normalization: https://github.com/ducha-aiki/caffenet-benchmark/blob/master/batchnorm.md
# CuDNN.... Use GPU implementations of .... model, this speeds up the training.
# """
# 
# from keras.layers import InputLayer, ConvLSTM2D, Reshape, GRU
# from keras.layers.normalization import BatchNormalization
# 
# """
# For info on batch normalization: https://github.com/ducha-aiki/caffenet-benchmark/blob/master/batchnorm.md
# CuDNN.... Use GPU implementations of .... model, this speeds up the training.
# """
# 
# from keras.layers import InputLayer, ConvLSTM2D, Reshape, GRU
# from keras.layers.normalization import BatchNormalization
# 
# def create_model(X_train, y_train, X_test, y_test):
#     input_shape = (X_train.shape[1], X_train.shape[2], X_train.shape[3])
# 
#     model = Sequential()
#     
#     ks1_first = 3
#     ks1_second = 8
#     
#     ks2_first = 4
#     ks2_second = 5
#     
#     model.add(Conv2D(filters=(3), 
#                      kernel_size=(ks1_first, ks1_second),
#                      input_shape=input_shape, 
#                      padding='same',
#                      kernel_initializer='TruncatedNormal'))
#     model.add(BatchNormalization())
#     model.add(LeakyReLU())
#     model.add(Dropout(0.025))
#     
#     for _ in range(2):
#         model.add(Conv2D(filters=(4), 
#                      kernel_size= (ks2_first, ks2_second), 
#                          padding='same',
#                      kernel_initializer='TruncatedNormal'))
#         model.add(BatchNormalization())
#         model.add(LeakyReLU())
#         model.add(Dropout(0.280))  
#     
#     model.add(Flatten())
#     
#     for _ in range(4):
#         model.add(Dense(64 , kernel_initializer='TruncatedNormal'))
#         model.add(BatchNormalization())
#         model.add(LeakyReLU())
#         model.add(Dropout(0.435))
#     
#     for _ in range(3):
#         model.add(Dense(128 , kernel_initializer='TruncatedNormal'))
#         model.add(BatchNormalization())
#         model.add(LeakyReLU())
#         model.add(Dropout(0.372))
#   
#     model.add(Dense(1024 , kernel_initializer='TruncatedNormal'))
#     model.add(BatchNormalization())
#     model.add(LeakyReLU())
#     model.add(Dropout(0.793))
#         
#     model.add(Dense(1))
#     
#     return model
# 
# 
# #print(model.summary())
# 
# #multi_model = multi_gpu_model(model, gpus=num_gpu)
# =============================================================================
