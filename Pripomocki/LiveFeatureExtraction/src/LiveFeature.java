import com.fazecast.jSerialComm.SerialPort;

public class LiveFeature {

    public static int port_id;

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please supply the serial port id as first parameter:");
            SerialPort available_ports[] = SerialPort.getCommPorts();

            for (int i = 0; i < available_ports.length; i++) {
                System.out.println(i + " : " + available_ports[i]);
            }
            //return;
        } else {
            port_id = Integer.parseInt(args[0]);
        }

        DataLogger dl = new DataLogger();
        dl.openSerialPort();
        dl.start();
    }
}
