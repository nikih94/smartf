import com.fazecast.jSerialComm.SerialPort;

import java.util.Scanner;

public class DataLogger
        extends Thread
{
    public static SerialPort port;
    public double delta = 3.5D;
    public FeatureExtraction theWindow;
    public Scanner s=null;


    public void openSerialPort() {
        SerialPort[] available_ports = SerialPort.getCommPorts();

        for (int i = 0; i < available_ports.length; i++) {
            System.out.println(String.valueOf(i) + " : " + available_ports[i]);
        }
        //kommentiraj za feature extraction iz file ------------------------------------------------------------------------
        port = available_ports[LiveFeature.port_id];
        if (port != null) {
            port.setComPortTimeouts(4096, 0, 0);
            System.out.println(port.openPort());
            if (!port.openPort()) {
                System.err.println("Unable to open the port.");
            }
        }
        //kommentiraj za feature extraction iz file ------------------------------------------------------------------------
        port.setBaudRate(115200);
    }

    public void run() {
        if (port != null) {
            float[] previous_datapoints = new float[16];
            Scanner s = new Scanner(port.getInputStream());
            theWindow = new FeatureExtraction(); //ustvarimo novo okno
            while (s.hasNextLine()) {
                String line = s.nextLine();
                line = line.substring(0, line.length() - 1);
                String[] datapoints = line.split(",");
                int [] featureArray = new int[16];
                if (datapoints.length > 16) {
                    System.out.println("Malformed payload by the controller");
                } else {
                    //nic feature extration
                    float[] currentDatapoints = new float[16];

                    for (int i = 0; i < 16 ; i++){
                        currentDatapoints[i] = Float.parseFloat(datapoints[i]);
                    }

                }
                System.out.println("Line of text from serial port: " + line);


            }
        } else {
            System.out.println("Unable to read from serial port..");
        }
    }

        /*
        //postavi port == null za feature extraction iz file, drugace port != null   --------------------------------------------------------
        if (port != null) {

            /// Branje iz ARDUINO
            s = new Scanner(port.getInputStream());
            System.out.println(s.nextLine());


            float[] previous_datapoints = new float[16];
            theWindow = new FeatureExtraction(); //ustvarimo novo okno


            System.out.println("working");

            while (s.hasNextLine()) {


                String line = s.nextLine();
                line = line.substring(0, line.length() - 1);

                String[] datapoints = line.split(",");
                int[] featureArray = new int[16];


                    if (datapoints.length > 16) {
                        System.out.println("Malformed payload by the controller");
                    } else {


                        theWindow.fill(datapoints);
                        featureArray = theWindow.extractFeatures();


                System.out.print(" features: ");

                for (int i = 0 ; i < featureArray.length ; i++){
                    System.out.print(Integer.toString(featureArray[i])+",");
                }
                System.out.println();

                }
            }
        } else {
            System.out.println("Unable to read from serial port..");
        }

    }

    */




    public void propagateForce(float[] sensor_data) {}
}